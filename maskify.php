<?php

function maskify($numbercard)
{
  if (strlen($numbercard) >= 12) {
    if (strpos($numbercard, '-')) {
      return $numbercard[0] . '###-####-' . substr($numbercard, -4, 4);
    } else {
      return $numbercard[0] . '#######' . substr($numbercard, -4, 4);
    }
  } else {
    return $numbercard;
  }
}
?>