<?php

class Clock

{
  private DateTime $time;
  public function __construct(int $hour = 0, int $minutes = 0)
  {
    $this->time = (new DateTime())->setTime($hour, $minutes)->setDate(0, 0, 0);
  }
  public function __toString()
  {
    return $this->time->format('H:i');
  }
  public function add(int $minutes): Clock
  {
    $this->time->modify("+{$minutes} minutes");
    return $this;
  }
  public function sub(int $minutes): Clock
  {
    $this->time->modify("-{$minutes} minutes");
    return $this;
  }
}
