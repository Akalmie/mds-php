<?php

class DndCharacter
{
  public int $hitpoints = 10;
  public int $ability;
  public int $strength;
  public int $dexterity;
  public int $constitution;
  public int $intelligence;
  public int $wisdom;
  public int $charisma;

  public function __construct()
  {
    $this->ability = $this->rollDices();
    $this->strength = $this->rollDices();
    $this->dexterity = $this->rollDices();
    $this->constitution = $this->rollDices();
    $this->intelligence = $this->rollDices();
    $this->wisdom = $this->rollDices();
    $this->charisma = $this->rollDices();
    $this->hitpoints = $this->hitpoints + $this->calculateConstitutionModifier();
  }

  public static function generate(): DndCharacter
  {
    $character = new DndCharacter();
    return $character;
  }
  public static function ability()
  {
    $character = new DndCharacter();
    $ability = $character->ability;
    return $ability;
  }
  public static function modifier(int $modifier): int
  {
    return floor(($modifier - 10) / 2);
  }
  private function calculateConstitutionModifier(): int
  {
    return (int) floor(($this->constitution - 10) / 2);
  }
  private function rollDices(): int
  {
    $rolls = [];
    $rolls[] = $this->rollDice();
    $rolls[] = $this->rollDice();
    $rolls[] = $this->rollDice();
    $rolls[] = $this->rollDice();
    $min = min($rolls);
    $searchMinIndex = array_search($min, $rolls);
    unset($rolls[$searchMinIndex]);
    return array_sum($rolls);
  }

  private function rollDice(): int
  {
    return rand(1, 6);
  }
}
