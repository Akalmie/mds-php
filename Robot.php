<?php
class Robot
{
  private static $robots = [];
  private $thisName = "-";
  private $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  private function setName(): string
  {
    global $robots;
    while (true) {
      $str = $this->chars[rand(0, 25)] . $this->chars[rand(0, 25)] . rand(100, 999);
      if (!in_array($str, Robot::$robots)) {
        array_push(Robot::$robots, $str);
        return $str;
      }
    }
  }

  public function getName(): string
  {
    if ($this->thisName === "-") return ($this->thisName = $this->setName());
    return $this->thisName;
  }
  public function reset(): void
  {
    $this->thisName = "-";
  }
}
