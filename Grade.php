<?php
class School
{
  private array $school;
  public function add(string $name, int $grade): void
  {
    $this->school[$grade][] = $name;
    sort($this->school[$grade]);
  }
  public function grade($grade): array
  {
    return isset($this->school[$grade])
      ? $this->school[$grade]
      : [];
  }
  public function studentsByGradeAlphabetical(): array
  {
    ksort($this->school);
    return $this->school;
  }
}
