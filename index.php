<?php
require "helloWorld.php";
require "reverseString.php";
require "Robot.php";
require "maskify.php";
require "Clock.php";
require "Grade.php";
require "Matrix.php";
try {
    echo 'Current PHP version: ' . phpversion();
    echo '<br />';

    $host = 'db';
    $dbname = 'database';
    $user = 'user';
    $pass = 'pass';
    $dsn = "mysql:host=$host;dbname=$dbname;charset=utf8";
    $conn = new PDO($dsn, $user, $pass);

    echo 'Database connected successfully';
    /***** Exercice 1 *****/
    echo '<br /> <h2> Exercice 1 </h2>';
    echo helloWorld();

    /***** Exercice 2 *****/
    echo '<br /> <h2> Exercice 2 </h2>';
    echo reverseString("cool");

    /***** Exercice 3 *****/
    echo '<br /> <h2> Exercice 3 </h2>';
    $robot = (new Robot())->getName();
    echo $robot;

    /***** Exercice 4 *****/
    echo '<br /> <h2> Exercice 4 </h2>';
    echo maskify("1234-5678-9012");

    /***** Exercice 5 *****/
    echo '<br /> <h2> Exercice 5 </h2>';
    $add = (new Clock(10, 10))->add(20);
    echo $add;
    $sub = (new Clock(10, 10))->sub(20);
    echo "<br/>$sub";


} catch (\Throwable $t) {
    echo 'Error: ' . $t->getMessage();
    echo '<br />';
}
