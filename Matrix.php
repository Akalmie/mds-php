<?php
class Matrix
{
  private array $data;
  public function __construct(string $data)
  {
    $this->data = array_map(static fn ($row) => explode(' ', $row), explode("\n", $data));
  }
  public function getRow(int $row): array
  {
    return $this->data[$row - 1];
  }
  public function getColumn(int $col): array
  {
    return array_column($this->data, $col - 1);
  }
}
